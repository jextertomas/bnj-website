<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress_db' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'OM{?o(pNL`k@ZmW4YvMIM9!7+hjqDG1(wWV8BGK<[[6%CnY7xoy?84$QM:_)>[_i' );
define( 'SECURE_AUTH_KEY',  'OR.<TnN:c319qqEoBx[5GEkRae4G^|y>L)E^4Zu6]t%,I}Tl8}Bg3z.H5=kf)<js' );
define( 'LOGGED_IN_KEY',    'IqVEo#*p=7^~ykzN,59DD0]Gf[m-bx1AgfvB;51b2#e0N7L=uCL%i[6J@O*ou|sV' );
define( 'NONCE_KEY',        '`QV@pLoyN0aK!|K/5O-$iJ_$,|nzIAK?X};)XipOG`l&Blo`.t}N$9n-EdOI64i%' );
define( 'AUTH_SALT',        'K,;4DN(DeGAjaofdwM@}ovs aFE17$]y}AeksE>Z[=5Yf=PG{rlnr)@-.wbIX@Xa' );
define( 'SECURE_AUTH_SALT', 'B(x[XBE[I!yLXeXYD5,%`cKJ1On ]b,J(!>sc(}cKkNg<gh2s_,8&^:s&[ro##CE' );
define( 'LOGGED_IN_SALT',   '<._VAJ&m !@ks9*ag[ 7QvVTfg?}:/M@P$rsIIhr&@Gv)V-/%duNB<Xq E-(DPOV' );
define( 'NONCE_SALT',       'bMSJ*BzrN~Svmbe|T.n),JUS%nso9{mcC4zcayKXW%cji0ByFGekU+RFppQ>kXFn' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/documentation/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
