<?php get_header(); ?>
<?php
    $to = 'recipient@example.com';
    $subject = 'Subject of the Email';
    $message = 'This is the body of the email';
    $headers = array('Content-Type: text/html; charset=UTF-8');
    
    // Send email using wp_mail() Not working yet
    $sent = wp_mail($to, $subject, $message, $headers);

    if ($sent) {
        echo 'Email successfully sent!';
    } else {
        echo 'There was a problem sending the email.';
    }
?>
<div class="py-3">
        <div class="container">

            <div class="owl-logos owl-carousel">
                <div class="item">
                    <img src="<?php echo esc_url(get_template_directory_uri() . '/assets/images/logo-puma.png'); ?>" alt="Image" class="img-fluid">
                </div>
                <div class="item">
                    <img src="<?php echo esc_url(get_template_directory_uri() . '/assets/images/logo-adobe.png'); ?>" alt="Image" class="img-fluid">
                </div>
                <div class="item">
                    <img src="<?php echo esc_url(get_template_directory_uri() . '/assets/images/logo-google.png'); ?>" alt="Image" class="img-fluid">
                </div>
                <div class="item">
                    <img src="<?php echo esc_url(get_template_directory_uri() . '/assets/images/logo-paypal.png'); ?>" alt="Image" class="img-fluid">
                </div>
                <div class="item">
                    <img src="<?php echo esc_url(get_template_directory_uri() . '/assets/images/logo-adobe.png'); ?>" alt="Image" class="img-fluid">
                </div>
                <div class="item">
                    <img src="<?php echo esc_url(get_template_directory_uri() . '/assets/images/logo-google.png'); ?>" alt="Image" class="img-fluid">
                </div>


            </div>


        </div>

    </div>

    <!-- Services -->
    <div class="site-section">
        <div class="container">
            <div class="row mb-5">
                <div class="col-12 text-center" data-aos="fade-up">
                    <h2 class="heading font-weight-bold mb-3">Our Services</h2>
                </div>
            </div>
            <div class="row align-items-stretch">
                <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up">
                    <div class="unit-4 d-flex">
                        <div class="unit-4-icon mr-4">
                            <span class="feather-pen-tool"></span>
                        </div>
                        <div>
                            <h3>Web Designing</h3>
                            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia,
                                there live the blind texts. </p>
                            <p><a href="#">Learn More</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up" data-aos-delay="100">
                    <div class="unit-4 d-flex">
                        <div class="unit-4-icon mr-4">
                            <span class="feather-layers"></span>
                        </div>
                        <div>
                            <h3>IT Consultancy</h3>
                            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia,
                                there live the blind texts. </p>
                            <p><a href="#">Learn More</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up" data-aos-delay="200">
                    <div class="unit-4 d-flex">
                        <div class="unit-4-icon mr-4">
                            <span class="feather-layout"></span>
                        </div>
                        <div>
                            <h3>Web Development</h3>
                            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia,
                                there live the blind texts. </p>
                            <p><a href="#">Learn More</a></p>
                        </div>
                    </div>
                </div>


                <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up" data-aos-delay="300">
                    <div class="unit-4 d-flex">
                        <div class="unit-4-icon mr-4">
                            <span class="feather-life-buoy"></span>
                        </div>
                        <div>
                            <h3>Data Analysis</h3>
                            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia,
                                there live the blind texts. </p>
                            <p><a href="#">Learn More</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up" data-aos-delay="400">
                    <div class="unit-4 d-flex">
                        <div class="unit-4-icon mr-4">
                            <span class="feather-shopping-bag"></span>
                        </div>
                        <div>
                            <h3>Business Processes</h3>
                            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia,
                                there live the blind texts. </p>
                            <p><a href="#">Learn More</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up" data-aos-delay="500">
                    <div class="unit-4 d-flex">
                        <div class="unit-4-icon mr-4">
                            <span class="feather-smartphone"></span>
                        </div>
                        <div>
                            <h3>APIs</h3>
                            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia,
                                there live the blind texts. </p>
                            <p><a href="#">Learn More</a></p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- Testimonials -->
    <div class="testimonial-section">
        <div class="container">
            <div class="row align-items-center justify-content-between">
                <div class="col-lg-4 mb-5 section-title" data-aos="fade-up" data-aos-delay="0">

                    <h2 class="mb-4 font-weight-bold heading">Testimonials</h2>
                    <p class="mb-4">Far far away, behind the word mountains, far from the countries Vokalia and
                        Consonantia, there live the blind texts. </p>
                    <p><a href="#" class="btn btn-primary">Home</a></p>
                </div>
                <div class="col-lg-7" data-aos="fade-up" data-aos-delay="100">

                    <div class="testimonial--wrap">
                        <div class="owl-single owl-carousel no-dots no-nav">

                            <div class="testimonial-item">
                                <div class="d-flex align-items-center mb-4">
                                    <div class="photo mr-3">
                                        <img src="<?php echo esc_url(get_template_directory_uri() . '/assets/images/person_2-min.jpg'); ?>" alt="Image" class="img-fluid">
                                    </div>
                                    <div class="author">
                                        <cite class="d-block mb-0">Naotaka Murakami</cite>
                                        <span>CEO of Brain Network Japan</span>
                                    </div>
                                </div>
                                <blockquote>
                                    <p>&ldquo;Far far away, behind the word mountains, far from the countries Vokalia
                                        and Consonantia, there live the blind texts. Separated they live.&rdquo;</p>
                                </blockquote>
                            </div>

                            <div class="testimonial-item">
                                <div class="d-flex align-items-center mb-4">
                                    <div class="photo mr-3">
                                        <img src="<?php echo esc_url(get_template_directory_uri() . '/assets/images/person_1-min.jpg'); ?>" alt="Image" class="img-fluid">
                                    </div>
                                    <div class="author">
                                        <cite class="d-block mb-0">Fumihito Sakurai</cite>
                                        <span>CTO of Brain Network Japan</span>
                                    </div>
                                </div>
                                <blockquote>
                                    <p>&ldquo;Far far away, behind the word mountains, far from the countries Vokalia
                                        and Consonantia, there live the blind texts. Separated they live.&rdquo;</p>
                                </blockquote>
                            </div>

                            <div class="testimonial-item">
                                <div class="d-flex align-items-center mb-4">
                                    <div class="photo mr-3">
                                        <img src="<?php echo esc_url(get_template_directory_uri() . '/assets/images/person_4-min.jpg'); ?>" alt="Image" class="img-fluid">
                                    </div>
                                    <div class="author">
                                        <cite class="d-block mb-0">Irene Vera Cruz</cite>
                                        <span>COO of Brain Network Japan</span>
                                    </div>
                                </div>
                                <blockquote>
                                    <p>&ldquo;Far far away, behind the word mountains, far from the countries Vokalia
                                        and Consonantia, there live the blind texts. Separated they live.&rdquo;</p>
                                </blockquote>
                            </div>

                        </div>
                        <div class="custom-nav-wrap">
                            <a href="#" class="custom-owl-prev"><span class="icon-keyboard_backspace"></span></a>
                            <a href="#" class="custom-owl-next"><span class="icon-keyboard_backspace"></span></a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <!-- Contact Us -->
    <div class="site-section overlay site-cover-2" style="background-image: url('<?php echo esc_url(get_template_directory_uri() . '/assets/images/img_v_3-min.jpg'); ?>')">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 mx-auto text-center">
                    <h2 class="text-white mb-4">Email Us! :)</h2>
                    <form action="#" class="sign-up-form d-flex" data-aos="fade-up" data-aos-delay="200">
                        <input type="text" class="form-control" placeholder="Enter email address">
                        <input type="submit" class="btn btn-primary" value="Sign up">
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- About Us -->
    <div class="site-footer">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-lg-4">
                    <div class="widget">
                        <h3>About</h3>
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia,
                            there live the blind texts. Separated they live.</p>
                    </div>
                    <div class="widget">
                        <h3>Connect with us</h3>
                        <ul class="social list-unstyled">
                            <li><a href="#"><span class="icon-facebook"></span></a></li>
                            <li><a href="#"><span class="icon-twitter"></span></a></li>
                            <li><a href="#"><span class="icon-instagram"></span></a></li>
                            <li><a href="#"><span class="icon-dribbble"></span></a></li>
                            <li><a href="#"><span class="icon-linkedin"></span></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-12">
                            <div class="widget">
                                <h3>Navigations</h3>
                            </div>
                        </div>
                        <div class="col-6 col-sm-6 col-md-4">
                            <div class="widget">
                                <ul class="links list-unstyled">
                                    <li><a href="#">Home</a></li>
                                    <li><a href="#">Services</a></li>
                                    <li><a href="#">Work</a></li>
                                    <li><a href="#">Process</a></li>
                                    <li><a href="#">About Us</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-6 col-sm-6 col-md-4">
                            <div class="widget">
                                <ul class="links list-unstyled">
                                    <li><a href="#">Press</a></li>
                                    <li><a href="#">Blog</a></li>
                                    <li><a href="#">Contact</a></li>
                                    <li><a href="#">Support</a></li>
                                    <li><a href="#">Privacy</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-6 col-sm-6 col-md-4">
                            <div class="widget">
                                <ul class="links list-unstyled">
                                    <li><a href="#">Privacy</a></li>
                                    <li><a href="#">FAQ</a></li>
                                    <li><a href="#">Careers</a></li>
                                    <li><a href="#">Process</a></li>
                                    <li><a href="#">About Us</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row justify-content-center text-center copyright">
                <div class="col-md-8">
                    <p>Copyright &copy;
                        <script>document.write(new Date().getFullYear());</script>. All Rights Reserved. &mdash;
                        Designed with love by <a href="https://untree.co">Untree.co</a>
                        <!-- License information: https://untree.co/license/ -->
                    </p> </br>
                    <p> Dsitributed by <a href="https://themewagon.com">themewagon</a> </p>
                </div>
            </div>
        </div>
    </div>

    <div id="overlayer"></div>
    <div class="loader">
        <div class="spinner-border" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    </div>
<?php get_footer(); ?>